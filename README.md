# photo-share-application
## This projet uses these tools: 
FASTAPI, Pytest, MongoDB,  Gitlab Container Rigistry,  GKE(Google Kubernets Engie), Gitlab CI/CD, Docker  
 
## This projet is a photo share application
It has two major parts
1. The Photographer Service  
The Photographer service exposes a REST API. It is implemented with the Python FastAPI framework. The pytest framework is used to implement functional testing of the service. The Photographer service relies on a Mongo database to store the attributes of a photographer.  
1.1 The Files  
• photographer_service.py contains the definition of the python functions serving the HTTP requests to the endpoints of the Photographer service.  
• photographer_const.py is a helper module for the photographer_service.py module  
• photographer.py contains the definition of the Photographer class to save photographer documents in a Mongo collection.  
• test_photographer.py contains the definition of the functions for testing the service with pytest.  
• photographer_mongo_wrapper.py contains the functions wrapping the calls to the mongo functions.  
• robustify.py contains decorators for retrying calls to mongo functions.  
• conftest.py contains fixtures used by pytest.  
1.2 The REST API  
• /photographers  
  – POST to create a new photographer. It returns the URI of the photographer in the HTTP Location header field.  
  – GET, to obtain a Photographers object (array of PhotographerDigest and a boolean indicating if there are more photographers to return).  
• /photographer/{display_name}  
  – GET to obtain the JSON object mapping photographer attributes names to their values for a given photographer (identified by display_name).  
  – DELETE, to delete a photographer identified by display_name.  
  – PUT, to update the attributes of a given photographer. 


2. The Photo Service (not finifhied)  
The Photo service must store photos (as jpeg files) and associated metadata. The metadata has the following fields (each field is optional except the author):  
• title (string)    
• location (string)  
• author (string, corresponding to the photographer display_name)   
• comment (string)  
• tags (list of strings)  
2.1 The Files  
Use the same file structure sa the Photographer Service.    
2.2 The REST API  
• /gallery/{display_name}    
  – POST, to upload a new photo for the photographer identified by display_name. It returns the URI of the photo in the HTTP Location header field. The URI of the photo must be of the following form: /photo/{display_name}/{photo_id}, photo_id being a unique identifier (for the photographer identified by display_name).  
  – GET, to obtain a JSON array of the URIs for the photos taken by the photographer identified by display_name.  
• /photo/{display_name}/{photo_id}  
  – GET, to obtain the photo identified by photo_id taken by the photographer identified by display_name.  
  – DELETE, to delete the photo identified by photo_id taken by the photographer identified by display_name.  
• /photo/{display_name}/{photo_id}/attributes  
  – GET, to obtain the attributes of the photo identified by photo_id taken by the photographer identified by display_name.  
  – PUT, to set the attributes the photo identified by photo_id taken by the photographer identified by display_name.  

## Result of projet:
### The Pipeline of Gitlab
![pipeline](https://gitlab.com/xiangpingjiang1998/devops-s20-02/-/blob/master/images/pipeline.png)  
We can see that the three jobs (test,release,deploy) have been executed successfully.  
### Pod in the GKE  
![GKE](https://gitlab.com/xiangpingjiang1998/devops-s20-02/-/blob/master/images/GKE.png)  
We can see the pod photographer-test2 has been started successfully in the cluster of k8s GKE.  
testxax
  

  


