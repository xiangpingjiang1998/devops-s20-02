#!/usr/bin/env python3

import logging
import json

from photo import MongoPhotographer, MongoPhoto, Pic
from mongoengine import *
import socket
import pymongo

from bson.objectid import ObjectId
from bson import json_util
from bson.errors import InvalidId


import json
import robustify
import requests
import os

logging.basicConfig(level=logging.DEBUG)

@robustify.retry_mongo
def mongo_check(display_name):
    count = MongoPhotographer.objects(display_name=display_name).count()
    return count

@robustify.retry_mongo
def mongo_add(display_name, first_name, last_name, interests):
    ph = MongoPhotographer(display_name = display_name,
                           first_name = first_name,
                           last_name = last_name,
                           interests = interests).save()
    return ph

@robustify.retry_mongo
def mongo_get_photographers(offset, limit):
    qs = MongoPhotographer.objects.order_by('id').skip(offset).limit(limit)
    if qs.count(with_limit_and_skip = False) > (offset + limit):
        has_more = True
    else:
        has_more = False

    return (has_more, qs)


@robustify.retry_mongo
def photographer_existe(name):
    phtographer_url = "http://0.0.0.0:8090"
    resp_services = requests.get("{}/photographers?offset=0&limit=20".format(phtographer_url))
    resp_services_json = resp_services.json()
    photographers = resp_services_json["items"]
    for photographer in photographers:
        if name == photographer["display_name"]:
            return True
    return False




@robustify.retry_mongo
def mongo_get_photos(offset, limit, name):
    qs = Pic.objects(author=name).order_by('id').skip(offset).limit(limit)
    if qs.count(with_limit_and_skip = False) > (offset + limit):
        has_more = True
    else:
        has_more = False

    return (has_more, qs)

@robustify.retry_mongo
def mongo_get_photographer_by_id(photographer_id):
    ph = MongoPhotographer.objects(id=ObjectId(photographer_id)).get()
    return ph

@robustify.retry_mongo
def mongo_get_photographer_by_name(name):
    ph = MongoPhotographer.objects(display_name=name).get()
    return ph

@robustify.retry_mongo
def mongo_get_photo_by_name_id(name,p_id):
    pi = Pic.objects(author=name,photo_id=p_id).get()
    return pi

@robustify.retry_mongo
def mongo_delete_photo_by_name_id(name,p_id):
    pi = Pic.objects(author=name,photo_id=p_id).get()
    pi.delete()
    return True



@robustify.retry_mongo
def mongo_delete_photographer_by_name(name):
    try:
        ph = MongoPhotographer.objects(display_name=name).get()
    except (MongoPhotographer.DoesNotExist,
            MongoPhotographer.MultipleObjectsReturned):
        return False
    except (pymongo.errors.AutoReconnect,
            pymongo.errors.ServerSelectionTimeoutError,
            pymongo.errors.NetworkTimeout) as e:
        raise 
    ph.delete()
    return True
    
@robustify.retry_mongo
def mongo_update_photographer(display_name, new_attributes):
    # new_attributes est un dict qui contient les nouvelles valeurs des differents champs à modifier
    try:
        ph_to_update = MongoPhotographer.objects(display_name=display_name).get()
        ph_to_update.update(**new_attributes)
        return True
    except Exception:
        return False