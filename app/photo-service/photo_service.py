#!/usr/bin/env python3

import uvicorn

from fastapi import Depends, FastAPI, HTTPException
from fastapi import File, UploadFile
from starlette.responses import Response

from mongoengine import connect
from starlette.requests import Request
from pydantic import BaseModel
from typing import List

from photo_mongo_wrapper import *
import pymongo
import requests
from photo_const import Dname, Photographer, PHOTOGRAPHER_BODY, Photographers, Photos
from photo import MongoPhotographer, MongoPhoto, Pic
import re

from bson import binary
import bson
import base64
# OK for testing or for running service
app = FastAPI(title = "Photo Service", debug = True)

@app.on_event("startup")
def startup_event():
    #connect("photographers", host="mongo-service")
    # change fore monge    
    connect('photo', host='172.17.0.3', port=27017)



@app.post("/gallery/{display_name}", status_code=201)
def upload_photo(response: Response, display_name: str = Dname.PATH_PARAM,file: UploadFile=File(...),photo_id: str = "0", photo_title: str = "title"):
    print ("upload_photo")
    if(photographer_existe(display_name)):
        pic = Pic()
        pic.image_file.put(file.file)
        pic.author = display_name
        pic.photo_id = photo_id
        pic.title = photo_title
        pic.save()
        response.headers["Location"] = "/photo/" + str(display_name)+"/" + str(photo_id)
    else:
        return "photographer not existe "


@app.get("/gallery/{display_name}", response_model = Photos, status_code = 200)    
def get_photos(request: Request, display_name: str = Dname.PATH_PARAM,offset: int = 0, limit: int = 10):
    print ("get_photos")
    list_of_photos = list()
    try:
        (has_more, photos) = mongo_get_photos(offset, limit, display_name)
        for pi in photos:
            pi._data['link'] = "http://" + request.headers['host'] + "/photo/" + str(pi.author)+"/" + str(pi.photo_id)
            list_of_photos.append(pi._data)
    except pymongo.errors.ServerSelectionTimeoutError:
        raise HTTPException(status_code=503, detail="Mongo unavailable")
    return {'items': list_of_photos, 'has_more': has_more}



@app.get("/photo/{display_name}/{photo_id}",  status_code=200)
def get_photo(display_name: str = Dname.PATH_PARAM, photo_id: str = "0"):
    print ("get_photo")
    try:
        pi = mongo_get_photo_by_name_id(display_name, photo_id)
    except (MongoPhotographer.DoesNotExist, InvalidId):
        raise HTTPException(status_code = 404, detail = "Photographer does not exist")
    except pymongo.errors.ServerSelectionTimeoutError:
        raise HTTPException(status_code = 503, detail = "Mongo unavailable")
    return   Response(content=pi.image_file.read(), media_type="image/jpeg")


@app.get("/photo/{display_name}/{photo_id}/attributes",  status_code=200)
def get_photo_attributes(display_name: str = Dname.PATH_PARAM, photo_id: str = "0"):
    print ("get_photo_attributes")
    try:
        pi = mongo_get_photo_by_name_id(display_name, photo_id)
    except (MongoPhotographer.DoesNotExist, InvalidId):
        raise HTTPException(status_code = 404, detail = "Photographer does not exist")
    except pymongo.errors.ServerSelectionTimeoutError:
        raise HTTPException(status_code = 503, detail = "Mongo unavailable")
    return   pi.title

@app.put("/photo/{display_name}/{photo_id}/attributes",  status_code=207)
def update_photo_attributes(display_name: str = Dname.PATH_PARAM, photo_id: str = "0", photo_title: str = "new title"):
    print ("update_photo_attributes")
    try:
        pi = mongo_get_photo_by_name_id(display_name, photo_id)
        pi.title = photo_title
        pi.save()
    except (MongoPhotographer.DoesNotExist, InvalidId):
        raise HTTPException(status_code = 404, detail = "Photographer does not exist")
    except pymongo.errors.ServerSelectionTimeoutError:
        raise HTTPException(status_code = 503, detail = "Mongo unavailable")



@app.delete("/photo/{display_name}/{photo_id}", status_code = 204)    
def delete_photo(display_name: str = Dname.PATH_PARAM, photo_id: str = "0"):
    pi = mongo_delete_photo_by_name_id(display_name, photo_id)
    





if __name__ == "__main__":
    # if we run the main, the uvicorn.run() below will be executed
    # if we run the container from the docker image specifically made for
    #   fastAPI, there is no need to execute the main (uvicorn
    #   is run by the docker base image).
    uvicorn.run(app, host="0.0.0.0", port=8091, log_level="info")
